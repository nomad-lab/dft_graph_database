from neomodel import config # type: ignore # pylint: disable=import-error,missing-module-docstring
from pathlib import Path
import yaml
from to_db import post
from general_parser import Triple, trace_files
from maple_parser import handler_maple
from libxc_c_parser import handler_libxc_c


def get_neo4j_settings() -> tuple[str, str, str, str]:
    with open("config.yaml", "r") as file:
        config = yaml.safe_load(file)
    file.close()
    return (
        config["neo4j"]["user"],
        config["neo4j"]["password"],
        config["neo4j"]["host"],
        config["neo4j"]["port"],
    )


def get_libxc_settings() -> tuple[str, str]:
    with open("config.yaml", "r") as file:
        config = yaml.safe_load(file)
    file.close()
    return config["libxc"]["path"], config["libxc"]["maple"]


def handler_composite(file_path: str) -> set[Triple]:
    '''Handle the extraction and processing of information from a file.'''
    if file_path.endswith(".mpl"):
        return handler_maple(file_path)
    if file_path.endswith(".c"):
        return handler_libxc_c(file_path)
    return set()


def __main__():
    # set up Neo4j connection
    config.DATABASE_URL = "bolt://{}:{}@{}:{}".format(*get_neo4j_settings())

    # Parse the Maple files
    maple_dir = "{}/".format(*get_libxc_settings())
    code_files = [str(file).split("/")[-1] for file in Path(maple_dir).rglob("*.mpl")]
    code_files += [str(file).split("/")[-1] for file in Path(maple_dir).rglob("*.c")]

    for code_file in code_files:
        print(f"Processing {code_file}...")
        triples = trace_files(code_file, maple_dir, handler_composite, triples=set())
        for triple in triples:
            post(triple)


# execute the main function
if __name__ == "__main__":
    __main__()
