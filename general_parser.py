import os
from typing import Callable, Optional

# Type aliases for clarity
Triple = tuple[str, str, str]
FileName = str
FileContents = list[str]
Block = list[str]
StartCondition = Callable[[str], bool]
EndCondition = Callable[[str], bool]

NamedBlock = tuple[Block, str]
FileProcessor = Callable[[FileName], list[NamedBlock]]
BlockProcessor = Callable[[Block], Triple]
FileHandler = Callable[[FileName], set[Triple]]


def file_path_to_name(file_path: str) -> str:
    return file_path.replace('//', '/').split("/")[-1]

def read_file(file_path: str, line_processor: Callable[[str], str]) -> FileContents:
    with open(file_path, "r") as file:
        contents = [line_processor(line) for line in file.readlines()]
    file.close()
    return contents


def clean_line(line: str) -> str:
    return line.strip("\n").strip()


def process_lines(
    lines: FileContents,
    start_condition: Optional[StartCondition] = None,
    end_condition: Optional[EndCondition] = None,
) -> tuple[FileContents, list[Block]]:
    '''Process a file (as a list of lines) into blocks based on start and end conditions.'''
    def default_start_condition(_):
        return True  # Start immediately

    def default_end_condition(_):
        return True  # End immediately after start

    # Use provided conditions or defaults
    start = start_condition or default_start_condition
    end = end_condition or default_end_condition

    def _process(
        lines: FileContents, start_processing: bool
    ) -> tuple[FileContents, list[Block]]:
        remaining: FileContents = []
        processed: list[Block] = []

        for i, line in enumerate(lines):
            if not start_processing and start(line):
                start_processing = True
                remaining_segment, segment = _process(lines[i:], True)
                remaining.extend(remaining_segment)
                processed.extend(segment)
                break

            if start_processing:
                if end(line):
                    processed.append([line.strip().strip("\n")] + lines[:i])
                    remaining = lines[i + 1 :]
                    more_remaining, more_segments = _process(remaining, False)
                    remaining = more_remaining
                    processed.extend(more_segments)
                    break
                processed.append([line])

        return remaining, processed

    return _process(lines, False)


def find_file(root_directory: str, file_reference: str) -> Optional[str]:
    for dirpath, _, filenames in os.walk(root_directory):
        if file_reference in filenames:
            return os.path.join(dirpath, file_reference)
    return None


def is_file_reference(triple: Triple) -> bool:
    return triple[2] == "FileReference"


def trace_files(
    file_name: str,
    search_root: str,
    file_handler: FileHandler,  # for multiple file_handlers, use a mock function
    triples: set[Triple],
) -> set[Triple]:
    """Recursively find and present files to the `file_handler`.
    If a new filename is found, search and repeat."""
    file_path = find_file(search_root, file_name)
    if file_path is not None:
        # Get new triples from the file handler
        new_triples = file_handler(file_path)

        # Add the new triples to the existing set
        updated_triples = triples.union(new_triples)

        # Extract new file references from the triples
        new_file_references = {
            triple[1] for triple in new_triples if is_file_reference(triple)
        }  # file reference triple: (referencing file, referenced file, FileReference)

        # Recursively update the set with new file references
        for new_file_reference in new_file_references:
            if new_file_reference not in {triple[1] for triple in triples}:
                updated_triples = updated_triples.union(
                    trace_files(
                        new_file_reference, search_root, file_handler, updated_triples
                    )
                )

    else:
        # If file_path is None, return the original set of triples
        updated_triples = triples

    return updated_triples
