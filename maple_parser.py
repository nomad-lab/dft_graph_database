import re
from typing import Optional
from general_parser import (
    Triple,
    Block,
    NamedBlock,
    process_lines,
    read_file,
    clean_line,
    file_path_to_name,
)


MapleFormula = tuple[str, list[str], str]  # (name, arguments, body)


# matching functions
def is_comment_start(line: str) -> bool:
    return line.startswith("(*")


def is_comment_end(line: str) -> bool:
    return line.strip().strip("\n").endswith("*)")


def is_maple_file_reference(line: str) -> bool:
    return line.startswith("$include")


def is_formula_start(line: str) -> bool:
    return ":=" in line


def is_formula_end(line: str) -> bool:
    return line.strip().strip("\n").endswith(":")


# Processing functions
def process_file_reference(file_name: str, lines: Block) -> Optional[Triple]:
    match = re.search(r'\$include\s+"(.*)"', lines[0])
    if match:
        return (file_name, match.group(1), "FileReference")
    return None


def process_formula_reference(file_name: str, lines: Block) -> Optional[Triple]:
    return (file_name, " ".join(lines).strip(":"), "FormulaReference")


def split_formula(formula: str) -> MapleFormula:
    func, body = formula.rsplit(r"\s+->\s+", 1)
    name, arguments = func.rsplit(r"\s+:=\s+", 1)
    return name, arguments.strip("()").split(","), body


# Composed, ready-made functions
def handler_maple(
    file_path: str,
) -> set[Triple]:  # decide on how to handle file_name rather than file_path
    '''Direct the extraction and processing of information from a Maple file.'''
    contents, file_name = read_file(file_path, clean_line), file_path_to_name(file_path)
    triples: set[Triple] = set()
    named_blocks: list[NamedBlock] = []

    # extract file references
    contents, blocks = process_lines(contents, is_maple_file_reference, None)
    for block in blocks:
        if (triple := process_file_reference(file_name, block)) is not None:
            triples.add(triple)
    # extract formulas
    contents, blocks = process_lines(contents, is_formula_start, is_formula_end)
    named_blocks.extend([(block, "MapleFormula") for block in blocks])
    for block in blocks:
        if (triple := process_formula_reference(file_name, block)) is not None:
            triples.add(triple)
    # TODO: extract comments
    return triples
