import re
from typing import Optional
from general_parser import (
    Triple,
    Block,
    read_file,
    clean_line,
    process_lines,
    file_path_to_name,
)


# matching functions
def is_file_reference(line: str) -> bool:
    return line.startswith("#include")


def is_description_start(line: str) -> bool:
    return line.startswith("const xc_func_info_type")


def is_description_end(line: str) -> bool:
    return line.endswith("};")


def is_function_redefinition_start(line: str) -> bool:
    return line.startswith("static void")


def is_function_redefinition_end(line: str) -> bool:
    return line.endswith("}")


# Processing functions
def process_file_reference(file_name: str, lines: Block) -> Optional[Triple]:
    match = re.search(r'#include\s+"(.*)"', lines[0])
    if match:
        target_file_name = match.group(1)
        if target_file_name.startswith("maple2c"):
            target_file_name = "/".join(
                [
                    f
                    for f in target_file_name.replace("2c", "")
                    .replace(".c", ".mpl")
                    .split("/")
                    if f
                ]
            )
        return (file_name, target_file_name, "FileReference")
    return None


# Composed functions
def handler_libxc_c(file_path: str) -> set[Triple]:
    """Handle the Libxc C files and return triples"""
    contents, file_name = read_file(file_path, clean_line), file_path_to_name(file_path)
    triples: set[Triple] = set()

    # extract file references
    contents, blocks = process_lines(contents, is_file_reference, None)
    for block in blocks:
        if triple := process_file_reference(file_name, block):
            triples.add(triple)
    return triples
