from neomodel import StructuredNode, StringProperty, RelationshipTo  # type: ignore # pylint: disable=import-error,missing-module-docstring
from general_parser import Triple


class FileNode(StructuredNode):
    name = StringProperty(unique_index=True, required=True)
    # parameters defined?
    references = RelationshipTo("FileNode", "references")


def file_path_to_name(file_path: str) -> str:
    return file_path.split("/")[-1]


def post_file_reference(file_path_1: str, file_path_2: str) -> None:
    file_1 = FileNode.get_or_create(
        {"path": file_path_1, "name": file_path_to_name(file_path_1)}
    )[0]
    file_2 = FileNode.get_or_create(
        {"path": file_path_2, "name": file_path_to_name(file_path_2)}
    )[0]
    file_1.references.connect(file_2)


def post(triple: Triple):
    if triple[2] == "FileReference":
        post_file_reference(triple[0], triple[1])
