import pytest
import yaml
from general_parser import trace_files
from maple_parser import handler_maple

# Read the YAML file
with open("config.yaml", "r") as file:
    config = yaml.safe_load(file)
file.close()
libxc_src = config["libxc"]["path"]
gga_exc = f"{libxc_src}/maple/gga_exc/"


@pytest.fixture
def gga_k_apbe():
    with open(f"{libxc_src}/maple/gga_exc/gga_k_apbe.mpl", "r") as file:
        contents = [line.strip("\n") for line in file.readlines()]
    file.close()
    return contents


@pytest.mark.parametrize(
    "file_name, search_root, expected",
    [
        (
            "gga_k_apbe.mpl",
            gga_exc,
            {("gga_k_apbe.mpl", "gga_x_pbe.mpl", "FileReference")},
        ),  # serial example
        (
            "gga_x_bpccac.mpl",
            gga_exc,
            {
                ("gga_x_bpccac.mpl", "gga_x_pbe.mpl", "FileReference"),
                ("gga_x_bpccac.mpl", "gga_x_pw91.mpl", "FileReference"),
            },
        ),  # parallel example
    ],
)
def test_recursion(file_name: str, search_root: str, expected: list[str]):
    assert trace_files(file_name, search_root, handler_maple, set()) == expected
