import pytest
import yaml
from general_parser import process_lines
from maple_parser import (
    is_comment_end,
    is_comment_start,
    is_formula_end,
    is_formula_start,
    is_maple_file_reference,
)

# Read the YAML file
with open("config.yaml", "r") as file:
    config = yaml.safe_load(file)
file.close()
libxc_src = config["libxc"]["path"]


@pytest.fixture
def gga_k_apbe():
    with open(f"{libxc_src}/maple/gga_exc/gga_k_apbe.mpl", "r") as file:
        contents = [line.strip("\n") for line in file.readlines()]
    file.close()
    return contents


def test_comment(gga_k_apbe):
    _, comment_line = process_lines([gga_k_apbe[8]], is_comment_start, is_comment_end)
    assert comment_line[0][0] == "(* type: gga_exc *)"

    _, comment_block = process_lines(gga_k_apbe[9:17], is_comment_start, is_comment_end)
    assert len(comment_block) == 6
    assert comment_block[0][0] == "(* prefix:"
    assert comment_block[-1][0] == "*)"


def test_parse_reference(gga_k_apbe):
    _, file_reference = process_lines([gga_k_apbe[16]], is_maple_file_reference, None)
    assert file_reference[0][0] == '$include "gga_x_pbe.mpl"'


def test_parse_formula(gga_k_apbe):
    _, formula = process_lines([gga_k_apbe[18]], is_formula_start, is_formula_end)
    assert (
        formula[0][0]
        == "f := (rs, z, xt, xs0, xs1) -> gga_kinetic(pbe_f, rs, z, xs0, xs1):"
    )
